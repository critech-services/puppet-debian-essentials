require 'spec_helper'
describe 'debian_essentials' do

  context 'with default values for all parameters' do
    it { should contain_class('debian_essentials') }
  end
end
