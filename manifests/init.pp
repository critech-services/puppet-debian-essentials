# Class: debian_essentials
# ===========================
#
# The class debian_essentials manage essentials packages and configurations for
# Debian Jessie. It's useful for setup a fresh server with common configurations.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `ssh_keys`
# An array of ssh keys to authorized access to root account.
#
# Examples
# --------
#
# @example
#    class { 'debian_essentials':
#      extra_packages => [
#           'fortune-mod',
#           'cowsay'
#      ],
#      ssh_keys => [
#           'ssh-rsa AAAAB3NzaC1yc2... key-1@domain',
#           'ssh-rsa Afw6jFER+IqFwC... key-2@domain'
#      ],
#    }
# Authors
# -------
#
# Mehdi Ghezal <mehdi.ghezal@critech-services.com>
#
# Copyright
# ---------
#
# Copyright 2016 Critech Services Limited.
#
class debian_essentials (
    Array $extra_packages = [],
    Array $ssh_keys = []
) {
    class { "debian_essentials::packages" :
        extra_packages => $extra_packages
    }

    class { "debian_essentials::configurations" :
        ssh_keys => $ssh_keys
    }
}
