# == Class: debian_essentials::packages
class debian_essentials::packages (
    Array $extra_packages = []
) {
    $packages = [
        'bash-completion',
        'ncdu',
        'incron',
        'pydf',
        'iotop',
        'htop',
        'iftop',
        'ntp',
        'ntpdate',
        'wget',
        'curl',
        'locate',
        'tree',
        'most'
    ]

    package { $packages:
        provider => 'apt',
        ensure => 'latest'
    }

    package { $extra_packages:
        provider => 'apt',
        ensure => 'latest'
    }
}
