# == Class: debian_essentials::configurations
class debian_essentials::configurations (
    Array $ssh_keys
) {
    file { "/root/.bashrc":
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/debian_essentials/bashrc/.bashrc',
        ensure => 'present',
        replace => true
    }

    file { "/root/.bashrc.d":
        ensure => 'directory',
        recurse => true,
        purge => true,
        force => true,
        owner => "root",
        group => "root",
        mode => '0644',
        source => "puppet:///modules/debian_essentials/bashrc/.bashrc.d",
    }

    file { "/root/.nanorc":
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/debian_essentials/nano/.nanorc',
        ensure => 'present',
        replace => true
    }

    file { "/root/.ssh/authorized_keys2":
        mode => "0600",
        owner => "root",
        group => "root",
        ensure => 'present',
        content => epp("debian_essentials/ssh/.ssh/authorized_keys2", {"ssh_keys" => $ssh_keys})
    }
}
