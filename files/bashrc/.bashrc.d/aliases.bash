# Useful Alias
alias ls="ls -lah --color=auto"
alias ll="ls -lah --color=auto"
alias l="ls -lah--color=auto"
alias nano="nano -c"
alias cd..="cd .."
alias ..="cd .."
alias grep="grep --color=auto"
alias mkdir="mkdir -p"
alias wget="wget -c"
alias df="df -Th --total"
alias du="du -ch"
alias top="htop"

# Utilities Alias
alias diskspace="du -S | sort -n -r |more"
