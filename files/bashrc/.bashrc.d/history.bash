# History
HISTFILESIZE=2000
HISTSIZE=2000
HISTCONTROL=ignoreboth
HISTIGNORE="ls:bg:fg:history:du"
HISTTIMEFORMAT="%F %T " # Timestamp

PROMPT_COMMAND="history -a" # Store history immediately

shopt -s histappend     # Append history instead of rewriting it
shopt -s cmdhist        # One command per line
